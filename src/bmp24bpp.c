#include <stdio.h>
#include <string.h>

#define RENDER_X_MAX  320
#define RENDER_Y_MAX  244

struct bmpdata{
	int x;
	int y;
};

//bitmap file header (14 bytes)
struct bitmap_file_header {
	unsigned short bitmap_type;	// 2 bytes
	int			  file_size;		// 4 bytes
	short		  reserved1;		// 2 bytes
	short		  reserved2;		// 2 bytes
	unsigned int  offset_bits;      // 2 bytes
};

//bitmap info header (40 bytes)
struct bitmap_info_header{
	unsigned int size_header;	// 4 bytes
	unsigned int width;			// 4 bytes
	unsigned int height;		// 4 bytes
	short int    planes;		// 4 bytes
	short int 	 bit_count;		// 4 bytes
	unsigned int compression;	// 4 bytes
	unsigned int image_size;    // 4 bytes
	unsigned int ppm_x;			// 4 bytes
	unsigned int ppm_y;			// 4 bytes
	unsigned int clr_used;		// 4 bytes
	unsigned int clr_important; // 4 bytes
};

int M[RENDER_Y_MAX][RENDER_X_MAX];
int N[RENDER_Y_MAX][RENDER_X_MAX];


void SaveBitMapfile(const char 		*file_name,			// File name
					struct bmpdata 	*bmp_data,			//Pixel Array
					int 			 bpp) 	    		//Bits per pixel
{
	
	FILE *image;
	int img_size = RENDER_X_MAX*RENDER_Y_MAX,
		padding_size = (4 - ((3*RENDER_X_MAX )% 4) ) % 4,	
		pixel_array_size = (3*RENDER_X_MAX + padding_size)*RENDER_Y_MAX,
		fileSize = 54 + pixel_array_size;
				
	int max_x, max_y, min_x, min_y;
	int i,j, size = 0;
	unsigned char color[3];
	
	struct bitmap_file_header bfh; //bitmap file header 
	struct bitmap_info_header bih; //bitmap info header

	
	bfh.bitmap_type	  = 0x4D42;
	bfh.file_size 	  = fileSize;	
	bfh.reserved1 	  = 0;
	bfh.reserved2 	  = 0;
	bfh.offset_bits   = 0;
	
	bih.size_header   = sizeof(bih);
	bih.width 		  = RENDER_X_MAX;
	bih.height		  = RENDER_Y_MAX;
	bih.planes 		  = 1;
	bih.bit_count	  = bpp;
	bih.compression   = 0;
	bih.image_size    = 0;
	bih.ppm_x         = 80;
	bih.ppm_y		  = 61;
	bih.clr_used	  = 0;
	bih.clr_important = 0;
	
	image = fopen(file_name, "wb");
	
	//compiler whoes so we will just use the constant 14 we know we have
	fwrite(&bfh, 1, 14, image);
	fwrite(&bih, 1, sizeof(bih), image);
	
	size = 0;
	max_x = bmp_data[size].x;
	min_x = bmp_data[size].x;
	max_y = bmp_data[size].y;
	min_y = bmp_data[size].y;
	while( size < 26){
		if((size +1) != 26){
			if(max_x < bmp_data[size + 1].x){
				max_x = bmp_data[size + 1].x;
			}	
			
			if(min_x > bmp_data[size + 1].x){
				min_x = bmp_data[size + 1].x;
			}
			
			if(max_y < bmp_data[size + 1].y){
				max_y = bmp_data[size + 1].y;
			}	
			
			if(min_y > bmp_data[size + 1].y){
				min_y = bmp_data[size + 1].y;
			}
		}
		
		size = size +1;
	}
	
	for(i = 0; i < RENDER_Y_MAX; i++ ){
		for(j = 0; j < RENDER_X_MAX; j++){
			size = 0;
			while( size < 26){
				if((bmp_data[size].x == i) && (bmp_data[size].y == j)){
					M[i][j] = 0x01;
					size = 25;
				}
				size = size + 1;
			}
			if(M[i][j] != 0x01){
				M[i][j] = 0xFF;
			}
		}
	}

	//put data in matrix pixel_data
	size = 0;
	for(i = 0; i < RENDER_Y_MAX; i++ ){
		for(j = 0; j < RENDER_X_MAX; j++){
			if((i>= min_y)&&(i <= max_y)&&(j>=min_x)&&(j<=max_y)){
				N[i][j] = M[j][i];
			}
			if(N[i][j] != 0x01){
				N[i][j] = 0xFF;
			}
						
			color[0] = N[i][j];
			color[1] = N[i][j];
			color[2] = N[i][j];
			
			fwrite(color, 1, sizeof(color), image);
			
			size = size +1;
		}   
	}
		
	fclose(image);
}

int main(){
	
	const char *imageBitmap = "firma24bpp.bmp";
	struct bmpdata bmp_data[4880];
	
	/********COORDINATES TO DRAW*****************/
	bmp_data[0].x  = 0;  bmp_data[0].y = 1;
	bmp_data[1].x  = 0;  bmp_data[1].y = 2;
	bmp_data[2].x  = 0;  bmp_data[2].y = 7;
	bmp_data[3].x  = 1;  bmp_data[3].y = 1;
	bmp_data[4].x  = 1;  bmp_data[4].y = 7;
	bmp_data[5].x  = 2;  bmp_data[5].y = 1;
	bmp_data[6].x  = 2;  bmp_data[6].y = 7;
	bmp_data[7].x  = 3;  bmp_data[7].y = 1;
	bmp_data[8].x  = 3;  bmp_data[8].y = 2;
	bmp_data[9].x  = 3;  bmp_data[9].y = 3;
	bmp_data[10].x = 3; bmp_data[10].y = 4;
	bmp_data[11].x = 3; bmp_data[11].y = 5;
	bmp_data[12].x = 3; bmp_data[12].y = 6;
	bmp_data[13].x = 3; bmp_data[13].y = 7;
	bmp_data[14].x = 4; bmp_data[14].y = 1;
	bmp_data[15].x = 4; bmp_data[15].y = 2;
	bmp_data[16].x = 4; bmp_data[16].y = 3;
	bmp_data[17].x = 4; bmp_data[17].y = 4;
	bmp_data[18].x = 4; bmp_data[18].y = 5;
	bmp_data[19].x = 4; bmp_data[19].y = 6;	
	bmp_data[20].x = 4; bmp_data[20].y = 7;
	bmp_data[21].x = 5;	bmp_data[21].y = 7;
	bmp_data[22].x = 6;	bmp_data[22].y = 7;
	bmp_data[23].x = 7;	bmp_data[23].y = 5;
	bmp_data[24].x = 7;	bmp_data[24].y = 6;
	bmp_data[25].x = 7;	bmp_data[25].y = 7;
	
	/********************************************/
		
	SaveBitMapfile(imageBitmap, bmp_data, 24);
	
	return 0;
}








