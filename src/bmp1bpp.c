#include <stdio.h>
#include <string.h>
#include <math.h>

#define RENDER_X_MAX  300
#define RENDER_Y_MAX  300

struct bmpdata{
	int x; //horizontal axis's component
	int y; //vertical axis's component
};

int M[RENDER_Y_MAX][RENDER_X_MAX];
int N[RENDER_Y_MAX][RENDER_X_MAX];

void SigCap2BMP(const char* file_name,
				struct bmpdata *bmp_data,
				int point_count, 
				int scala)
{
	int size = 0;

	FILE *image;

	unsigned long FileHeaderSize = 14,
				  InfoHeaderSize = 40,
				  PaletteSize = pow(2,1)*4;

	int img_size = RENDER_X_MAX*RENDER_Y_MAX;
	int max_x, max_y, min_x, min_y;
	int i, j;
	int size_real_x, size_real_y;
	int coord_x[20000], coord_y[20000], ind_new_points[20000];
	int count=0;
	int lost_points_x_right, lost_points_y_right,lost_points_x_left, lost_points_y_left;
	int ind_coord_x = 0, ind_coord_y=0;
	int a;

	//for(i=0;i<point_count;i++) printf("bmp_data[%d].x = %d; bmp_data[%d].y = %d; \n", i,bmp_data[i].x,i,bmp_data[i].y);

	for(i=0;i<20000;i++){
			coord_x[i] =0;
			coord_y[i] =0;
	}
	if(bmp_data[point_count-2].x >= 0 && bmp_data[point_count-2].y >= 0){
		bmp_data[point_count-1].x = bmp_data[point_count-2].x;
		bmp_data[point_count-1].y = bmp_data[point_count-2].y;
	}
	
	if(bmp_data[1].x >= 0 && bmp_data[1].y >= 0){
		bmp_data[0].x = bmp_data[1].x;
		bmp_data[0].y = bmp_data[1].y;
	}

	//for(i=0;i<point_count;i++) printf("bmp_data[%d].x = %d; bmp_data[%d] = %d;\n",i,bmp_data[i].x,i,bmp_data[i].y);
	/////////////// SEARCHING MAX & MIN COORDINATES////////////////////////
	size = 0;
	max_x = bmp_data[size].x;
	min_x = bmp_data[size].x;
	max_y = bmp_data[size].y;
	min_y = bmp_data[size].y;
	while( size < point_count){
		if(size!=point_count-2){
			if(max_x < bmp_data[size + 1].x && (bmp_data[size + 1].x > 0)){
				max_x = bmp_data[size + 1].x;
			}

			if(min_x > bmp_data[size + 1].x && (bmp_data[size + 1].x > 0)){
				min_x = bmp_data[size + 1].x;
			}

			if(max_y < bmp_data[size + 1].y && (bmp_data[size + 1].y > 0)){
				max_y = bmp_data[size + 1].y;
			}

			if(min_y > bmp_data[size + 1].y && (bmp_data[size + 1].y > 0)){
				min_y = bmp_data[size + 1].y;
			}
		}

		size = size +1;
	}

	for(i=0;i<point_count;i++){
		if(bmp_data[i].x==0 && bmp_data[i].y==0){
		}
		else{
			bmp_data[i].x = bmp_data[i].x - min_x;
			bmp_data[i].y = bmp_data[i].y - min_y;
		}
	}
	////////////////////////////////////////////////////////////////////////
	size_real_x = max_x - min_x;
	printf("min_x = %d, max_x = %d, size_real_x = %d\n",min_x,max_x,size_real_x);
	size_real_y = max_y - min_y;
	printf("jmauricio::min_x = %d, max_x = %d, min_y = %d, max_y = %d, size_real_x = %d, size_real_y = %d \n",min_x,max_x,min_y,max_y, size_real_x, size_real_y);
	//for(i=0;i<point_count;i++) printf("bmp_data[%d].x = %d; bmp_data[%d] = %d;\n",i,bmp_data[i].x,i,bmp_data[i].y);
	//////////////////////SCALE////////////////////////////////////////////
	for(i=0;i<point_count;i++){
			bmp_data[i].x=(int)((bmp_data[i].x*scala)/100);
			bmp_data[i].y=(int)((bmp_data[i].y*scala)/100);
	}

	max_x=(int)((max_x*scala)/100);
	min_x=(int)((min_x*scala)/100);
	max_y=(int)((max_y*scala)/100);
	min_y=(int)((min_y*scala)/100);
	size_real_x = (int)(max_x - min_x + 1);
	size_real_y = (int)(max_y - min_y + 1);

	printf("jmauricio::NEXT TO SCALE:: min_x = %d, max_x = %d, min_y = %d, max_y = %d, size_real_x = %d, size_real_y = %d \n",min_x,max_x,min_y,max_y, size_real_x, size_real_y);
	///////////////////////////////////////////////////////////////////////

	//////////////////////COMPLETE COORDINATES/////////////////////////////
	for(i=0;i<(point_count-1);i++){
		if(bmp_data[i].x > 0 && bmp_data[i].y > 0 && bmp_data[i+1].x >0 && bmp_data[i+1].y > 0){
			if(bmp_data[i+1].x - bmp_data[i].x >1){
				lost_points_x_right= bmp_data[i+1].x - bmp_data[i].x -1;
				for(j=0; j<lost_points_x_right;j++){
					coord_x[ind_coord_x + j] = bmp_data[i].x +1 + j;
					if(bmp_data[i+1].y - bmp_data[i].y >1){
						if((bmp_data[i].y +1 +j) < bmp_data[i+1].y){
							coord_y[ind_coord_y + j] = bmp_data[i].y +1 +j;
						}else{
							coord_y[ind_coord_y+j]=bmp_data[i+1].y;
						}
					}
					else if(bmp_data[i].y - bmp_data[i+1].y >1){
						if((bmp_data[i].y - 1 -j) > bmp_data[i+1].y){
							coord_y[ind_coord_y + j] = bmp_data[i].y -1 -j;
						}else{
							coord_y[ind_coord_y+j]=bmp_data[i+1].y;
						}
					}
					else{
						coord_y[ind_coord_y + j]=bmp_data[i].y;
					}
					//printf("i=%d; count=%d, coord_x[%d]=%d; coord_y[%d]=%d;\n",i,count,ind_coord_x+j,coord_x[ind_coord_x+j],ind_coord_y+j,coord_y[ind_coord_y+j]);
				}
				ind_coord_x+=lost_points_x_right; ind_coord_y+=lost_points_x_right;
				count+=lost_points_x_right;
			}
			if(bmp_data[i].x - bmp_data[i+1].x >1){
				lost_points_x_left= bmp_data[i].x - bmp_data[i+1].x - 1;
				for(j=0; j<lost_points_x_left;j++){
					coord_x[ind_coord_x+j]=bmp_data[i].x -1 -j;
					if(bmp_data[i+1].y - bmp_data[i].y >1){
						if((bmp_data[i].y +1 +j) < bmp_data[i+1].y){
							coord_y[ind_coord_y + j] = bmp_data[i].y +1 +j;
						}else{
							coord_y[ind_coord_y+j]=bmp_data[i+1].y;
						}
					}
					else if(bmp_data[i].y - bmp_data[i+1].y >1){
						if((bmp_data[i].y - 1 -j) > bmp_data[i+1].y){
							coord_y[ind_coord_y + j] = bmp_data[i].y -1 -j;
						}else{
							coord_y[ind_coord_y+j]=bmp_data[i+1].y;
						}
					}
					else{
						coord_y[ind_coord_y+j]=bmp_data[i].y;
					}
					//printf("i=%d; count=%d, coord_x[%d]=%d; coord_y[%d]=%d;\n",i,count,ind_coord_x+j,coord_x[ind_coord_x+j],ind_coord_y+j,coord_y[ind_coord_y+j]);
				}
				ind_coord_x+=lost_points_x_left; ind_coord_y+=lost_points_x_left;
				count+=lost_points_x_left;
			}
			if(bmp_data[i+1].y - bmp_data[i].y >1){
				lost_points_y_right= bmp_data[i+1].y - bmp_data[i].y - 1;
				for(j=0; j<lost_points_y_right;j++){
					coord_y[ind_coord_y+j]=bmp_data[i].y +1 +j;
					if(bmp_data[i+1].x - bmp_data[i].x >1){
						if((bmp_data[i].x +1 +j) < bmp_data[i+1].x){
							coord_x[ind_coord_x + j] = bmp_data[i].x +1 +j;
						}else{
							coord_x[ind_coord_x+j]=bmp_data[i+1].x;
						}
					}
					else if(bmp_data[i].x - bmp_data[i+1].x >1){
						if((bmp_data[i].x - 1 -j) > bmp_data[i+1].x){
							coord_x[ind_coord_x+j]=bmp_data[i].x -1 -j;
						}else{
							coord_x[ind_coord_x+j]=bmp_data[i+1].x;
						}
					}
					else{
						coord_x[ind_coord_x+j]=bmp_data[i].x;
					}
					//printf("i=%d; count=%d, coord_x[%d]=%d; coord_y[%d]=%d;\n",i,count,ind_coord_x+j,coord_x[ind_coord_x+j],ind_coord_y+j,coord_y[ind_coord_y+j]);
				}
				ind_coord_x+=lost_points_y_right; ind_coord_y+=lost_points_y_right;
				count+=lost_points_y_right;
			}
			if(bmp_data[i].y - bmp_data[i+1].y >1){
				lost_points_y_left= bmp_data[i].y - bmp_data[i+1].y - 1;
				for(j=0; j<lost_points_y_left;j++){
					coord_y[ind_coord_y +j]=bmp_data[i].y -1 -j;
					if(bmp_data[i+1].x - bmp_data[i].x >1){
						if((bmp_data[i].x +1 +j) < bmp_data[i+1].x){
							coord_x[ind_coord_x + j] = bmp_data[i].x +1 +j;
						}else{
							coord_x[ind_coord_x+j]=bmp_data[i+1].x;
						}
					}
					else if(bmp_data[i].x - bmp_data[i+1].x >1){
						if((bmp_data[i].x - 1 -j) > bmp_data[i+1].x){
							coord_x[ind_coord_x+j]=bmp_data[i].x -1 -j;
						}else{
							coord_x[ind_coord_x+j]=bmp_data[i+1].x;
						}
					}
					else{
						coord_x[ind_coord_x+j]=bmp_data[i].x;

					}

					//printf("i=%d; count=%d, coord_x[%d]=%d; coord_y[%d]=%d;\n",i,count,ind_coord_x+j,coord_x[ind_coord_x+j],ind_coord_y+j,coord_y[ind_coord_y+j]);
				}
				ind_coord_x+=lost_points_y_left; ind_coord_y+=lost_points_y_left;
				count+=lost_points_y_left;
			}
		}else{
			bmp_data[i].x=bmp_data[i+1].x;
			bmp_data[i].y=bmp_data[i+1].y;
		}
	}


	point_count+=count;
	printf("jmauricio::new coordinates to matrix data... ");
	/////////////////INSERTION OF NEW POINTS FOR COMPLETION/////////////////////////////////
	int coord_x_aux[point_count], coord_y_aux[point_count];
	int count_points, m=0, ind=0;

	for(i=0;i<point_count;i++){
		coord_x_aux[i]=0;
		coord_y_aux[i]=0;
	}

	a=0;
	for(i=0;i<point_count;i++){
		if(i+ind < point_count){
			coord_x_aux[i+ind]=bmp_data[i].x;
			coord_y_aux[i+ind]=bmp_data[i].y;
			if(i==ind_new_points[a]){
				m=0,count_points=0;
				for(j=0;j<count;j++){
					if(ind_new_points[j] == i){
						count_points+=1;
					}
				}
				while(m <count_points){
					coord_x_aux[i+m+a+1]=coord_x[a+m];
					coord_y_aux[i+m+a+1]=coord_y[a+m];
					m+=1;
				}
				a+=m;
				ind+=count_points;
			}
		}else{
			i=point_count-1;
		}
	}
	printf("COMPLETE! \n");
	
	////////////////////////////////////////////////////////////////////////
	printf("jmauricio::new coordinates in auxiliar matrix data... ");
	////////////////////ASSING NEW VALUES TO BITMAP STRUCT//////////////////
	for(i=0;i<point_count;i++){
		bmp_data[i].x=coord_x_aux[i];
		bmp_data[i].y=coord_y_aux[i];
		coord_x_aux[i]=0;
		coord_y_aux[i]=0;
	}
	printf("COMPLETADO! \n");
	
	///////////////////////////////////////////////////////////////////////
	printf("jmauricio::inserting data in auxiliar matrix... ");
	////////////////////DROP POINTS WITH TWO OR MORE SAME VALUES///////////

	i=1; j=1;
	coord_x_aux[0]=bmp_data[0].x;
	coord_y_aux[0]=bmp_data[0].y;
	while((i+1)!=point_count){
		if((bmp_data[i].x == bmp_data[i+1].x) && (bmp_data[i].y == bmp_data[i+1].y)){
		}
		else{
			coord_x_aux[j]=bmp_data[i].x;
			coord_y_aux[j]=bmp_data[i].y;
			j+=1;
		}
		i+=1;
	}
	for(i=0;i<j;i++){
		bmp_data[i].x=coord_x_aux[i];
		bmp_data[i].y=coord_y_aux[i];
	}

	point_count=j;
	printf("COMPLETE! \n");
	printf("jmauricio::same points deleted- coordinates cleaned...\n");
	
	
	for(i=0;i<point_count;i++) printf("bmp_data[%d].x = %d; bmp_data[%d] = %d;\n",i,bmp_data[i].x,i,bmp_data[i].y);
	////////////////////////////////////////////////////////////////////////

	unsigned long BytesPerRow = ( ( 1*size_real_x +31) / 32 )* 4;
	unsigned long BytesSize   = BytesPerRow*size_real_y;
	unsigned long fileSize 	  = FileHeaderSize + InfoHeaderSize + PaletteSize + BytesSize;
	unsigned long OffBits	  = FileHeaderSize + InfoHeaderSize + PaletteSize;
	unsigned char   byte[1];
	unsigned short  word[1]; /* 2 bytes */
    unsigned long  dword[1]; /* 4 bytes */
	unsigned char  bytes[BytesSize];
	unsigned char  vByte[img_size];

	image = fopen(file_name, "wb");

	// BITMAP FILE HEADER
	word [0] = 19778;                     fwrite( word,1,2,image);  /* file Type signature = BM */
    dword[0] = fileSize;                  fwrite(dword,1,4,image);  /* FileSize */
    word [0] = 0;                         fwrite( word,1,2,image);  /* reserved1 */
    word [0] = 0;                         fwrite( word,1,2,image);  /* reserved2 */
    dword[0] = OffBits;                   fwrite(dword,1,4,image);  /* OffBits */
    //BITMAP INFO HEADER
    dword[0] = InfoHeaderSize;            fwrite(dword,1,4,image);
    dword[0] = size_real_x;               fwrite(dword,1,4,image);
    dword[0] = size_real_y;               fwrite(dword,1,4,image);
    word [0] = 1;                         fwrite( word,1,2,image);  /* planes */
    word [0] = 1;                         fwrite( word,1,2,image);  /* Bits of color per pixel */
    dword[0] = 0;                         fwrite(dword,1,4,image);  /* compression type */
    dword[0] = 0;                         fwrite(dword,1,4,image);  /* Image Data Size, set to 0 when no compression */
    dword[0] = 0;                         fwrite(dword,1,4,image);  /* ppm_x */
    dword[0] = 0;                         fwrite(dword,1,4,image);  /* ppm_y */
    dword[0] = 2;                         fwrite(dword,1,4,image);  /* number of used colors*/
    dword[0] = 0;                         fwrite(dword,1,4,image);  /* number of used important colors*/

	//Color Table(Palette) = 2 colors as a RGBA

	//Color 0 = White
	byte[0] = 255;						  fwrite(byte, 1, 1,image);
	byte[0] = 255;						  fwrite(byte, 1, 1,image);
	byte[0] = 255;						  fwrite(byte, 1, 1,image);
	byte[0] = 255;						  fwrite(byte, 1, 1,image);
	//Color 1 = Black
	byte[0] = 0;						  fwrite(byte, 1, 1,image);
	byte[0] = 0;						  fwrite(byte, 1, 1,image);
	byte[0] = 0;						  fwrite(byte, 1, 1,image);
	byte[0] = 255;						  fwrite(byte, 1, 1,image);

	printf("jmauricio::cleaning pixel array... \n");


	int sizeV= RENDER_X_MAX*RENDER_Y_MAX;
	int indice;
	int indNX[20000], indNY[20000];
	int auxNX,auxNY;//, aux, res;
	
	for(i = 0; i < sizeV; i++)
		vByte[i] = 0;

	for(i=0;i<point_count;i++){
		indNX[i] = RENDER_X_MAX -bmp_data[i].y -1;//-1
		indNY[i] = bmp_data[i].x;
	}
	
	
	for (i = 0; i < point_count; ++i){
        for (j = i + 1; j < point_count; ++j){
            if(indNX[i] > indNX[j]){
            	auxNX = indNX[i];
            	auxNY = indNY[i];
				indNX[i] = indNX[j];
				indNY[i] = indNY[j];
                indNX[j] = auxNX;   
                indNY[j] = auxNY;
            }
        }
	}
	
	for(i=0;i<point_count;i++){
		for(j=i+1;j<point_count;j++){
			if(indNX[i]==indNX[j]){
				if(indNY[i] > indNY[j]){
					auxNY = indNY[i];
					indNY[i] = indNY[j];
					indNY[j] = auxNY;
				}
			}
		}
	}
   
	indice = indNY[1];
    
    printf("============================================================\n\n");
    
    for(i=1;i<point_count;i++){
		if(indNX[i] >= 0 && indNX[i] < RENDER_X_MAX && indNY[i]>= 0 && indNY[i] < RENDER_Y_MAX){
			vByte[indice] = 1;
			//printf("N[%d][%d] :: vByte[%d]\n",indNX[i], indNY[i], indice);
			if(indNX[i+1]>indNX[i]){
				indice = indice + (indNX[i+1] - indNX[i])*size_real_x + (indNY[i+1] - indNY[i]);
			}
			if(indNX[i+1] == indNX[i]){
				indice = indice + (indNY[i+1] - indNY[i]);
			}
		}
	}	
	
	printf("COMPLETE! \n\n\n");

	int nBytesRowReal = (size_real_x <= 8)? 1 : size_real_x/8 + ((size_real_x%8==0)?0:1); //Calculate the number of Bytes that will really use per row
	int bitInRow =0, nByte = 0, k = 0;
	printf("jmauricio::computing points...\n");
	size= 0;
	if(size_real_x <= 8){
		for(i = 0; i < size_real_y; i++){
			for(j=0 ; j < size_real_x; j++){
				bitInRow += pow(2,7-j)*vByte[size];	//at this point we caculate the bit combination of bytes in each row
				size+=1;
			}
			bytes[nByte] = bitInRow; //setting bit combination in row in the currently byte
			bitInRow = 0;
			nByte +=4;	// if RENDER_X_MAX is lower than 8, then, plus 4 'cause the number of bytes that we used is 1
		}
	}
	else{
		for(i = 0; i <size_real_y; i++){
			for(j = 0; j < size_real_x; j++){
				bitInRow += pow(2,7-k)*vByte[size]; //at this point we caculate the bit combination of bytes in each row
				if((k+1)%8 == 0){
					bytes[nByte] = bitInRow; //setting bit combination in row in the currently byte
					bitInRow = 0; k= (-1);
					nByte+=1;
				}
				k+=1; size+=1;
			}
			bytes[nByte] = bitInRow; //setting bit combination in row in the currently byte
			bitInRow = 0; k=0;
			nByte+=(size_real_x%8 == 0)?(BytesPerRow-nBytesRowReal):(BytesPerRow-nBytesRowReal +1);
		}
	}

	fwrite(bytes, 1, BytesSize,image);

	fclose(image);
}


int main(){
	
	const char *imageBitmap = "firma1bpp.bmp";
	struct bmpdata bmp_data[20000];
bmp_data[0].x = -1; bmp_data[0].y = -1;
bmp_data[1].x = 353; bmp_data[1].y = 362;
bmp_data[2].x = 353; bmp_data[2].y = 361;
bmp_data[3].x = 352; bmp_data[3].y = 361;
bmp_data[4].x = 353; bmp_data[4].y = 361;
bmp_data[5].x = 354; bmp_data[5].y = 362;
bmp_data[6].x = 355; bmp_data[6].y = 365;
bmp_data[7].x = 358; bmp_data[7].y = 369;
bmp_data[8].x = 360; bmp_data[8].y = 375;
bmp_data[9].x = 362; bmp_data[9].y = 379;
bmp_data[10].x = 365; bmp_data[10].y = 390;
bmp_data[11].x = 368; bmp_data[11].y = 414;
bmp_data[12].x = 368; bmp_data[12].y = 424;
bmp_data[13].x = 369; bmp_data[13].y = 449;
bmp_data[14].x = 369; bmp_data[14].y = 480;
bmp_data[15].x = 369; bmp_data[15].y = 498;
bmp_data[16].x = 368; bmp_data[16].y = 537;
bmp_data[17].x = 366; bmp_data[17].y = 580;
bmp_data[18].x = 365; bmp_data[18].y = 603;
bmp_data[19].x = 363; bmp_data[19].y = 648;
bmp_data[20].x = 360; bmp_data[20].y = 693;
bmp_data[21].x = 358; bmp_data[21].y = 736;
bmp_data[22].x = 356; bmp_data[22].y = 776;
bmp_data[23].x = 355; bmp_data[23].y = 795;
bmp_data[24].x = 354; bmp_data[24].y = 830;
bmp_data[25].x = 353; bmp_data[25].y = 861;
bmp_data[26].x = 353; bmp_data[26].y = 875;
bmp_data[27].x = 352; bmp_data[27].y = 900;
bmp_data[28].x = 351; bmp_data[28].y = 922;
bmp_data[29].x = 351; bmp_data[29].y = 930;
bmp_data[30].x = 350; bmp_data[30].y = 945;
bmp_data[31].x = 350; bmp_data[31].y = 954;
bmp_data[32].x = 350; bmp_data[32].y = 960;
bmp_data[33].x = 350; bmp_data[33].y = 963;
bmp_data[34].x = 349; bmp_data[34].y = 964;
bmp_data[35].x = 349; bmp_data[35].y = 963;
bmp_data[36].x = 348; bmp_data[36].y = 962;
bmp_data[37].x = 347; bmp_data[37].y = 959;
bmp_data[38].x = 346; bmp_data[38].y = 958;
bmp_data[39].x = 345; bmp_data[39].y = 955;
bmp_data[40].x = 343; bmp_data[40].y = 951;
bmp_data[41].x = 343; bmp_data[41].y = 944;
bmp_data[42].x = 342; bmp_data[42].y = 940;
bmp_data[43].x = 342; bmp_data[43].y = 930;
bmp_data[44].x = 342; bmp_data[44].y = 916;
bmp_data[45].x = -1; bmp_data[45].y = -1;
bmp_data[46].x = 322; bmp_data[46].y = 448;
bmp_data[47].x = 321; bmp_data[47].y = 447;
bmp_data[48].x = 320; bmp_data[48].y = 445;
bmp_data[49].x = 320; bmp_data[49].y = 444;
bmp_data[50].x = 320; bmp_data[50].y = 442;
bmp_data[51].x = 320; bmp_data[51].y = 437;
bmp_data[52].x = 322; bmp_data[52].y = 430;
bmp_data[53].x = 326; bmp_data[53].y = 421;
bmp_data[54].x = 333; bmp_data[54].y = 410;
bmp_data[55].x = 343; bmp_data[55].y = 397;
bmp_data[56].x = 356; bmp_data[56].y = 381;
bmp_data[57].x = 363; bmp_data[57].y = 373;
bmp_data[58].x = 388; bmp_data[58].y = 348;
bmp_data[59].x = 398; bmp_data[59].y = 340;
bmp_data[60].x = 417; bmp_data[60].y = 325;
bmp_data[61].x = 437; bmp_data[61].y = 313;
bmp_data[62].x = 456; bmp_data[62].y = 304;
bmp_data[63].x = 474; bmp_data[63].y = 299;
bmp_data[64].x = 490; bmp_data[64].y = 298;
bmp_data[65].x = 505; bmp_data[65].y = 299;
bmp_data[66].x = 517; bmp_data[66].y = 304;
bmp_data[67].x = 523; bmp_data[67].y = 308;
bmp_data[68].x = 537; bmp_data[68].y = 324;
bmp_data[69].x = 544; bmp_data[69].y = 338;
bmp_data[70].x = 547; bmp_data[70].y = 346;
bmp_data[71].x = 550; bmp_data[71].y = 365;
bmp_data[72].x = 552; bmp_data[72].y = 385;
bmp_data[73].x = 550; bmp_data[73].y = 407;
bmp_data[74].x = 546; bmp_data[74].y = 431;
bmp_data[75].x = 542; bmp_data[75].y = 443;
bmp_data[76].x = 533; bmp_data[76].y = 467;
bmp_data[77].x = 520; bmp_data[77].y = 491;
bmp_data[78].x = 513; bmp_data[78].y = 504;
bmp_data[79].x = 496; bmp_data[79].y = 530;
bmp_data[80].x = 477; bmp_data[80].y = 556;
bmp_data[81].x = 458; bmp_data[81].y = 582;
bmp_data[82].x = 447; bmp_data[82].y = 595;
bmp_data[83].x = 427; bmp_data[83].y = 619;
bmp_data[84].x = 409; bmp_data[84].y = 640;
bmp_data[85].x = 392; bmp_data[85].y = 659;
bmp_data[86].x = 378; bmp_data[86].y = 673;
bmp_data[87].x = 372; bmp_data[87].y = 678;
bmp_data[88].x = 363; bmp_data[88].y = 686;
bmp_data[89].x = 357; bmp_data[89].y = 691;
bmp_data[90].x = 356; bmp_data[90].y = 692;
bmp_data[91].x = 354; bmp_data[91].y = 692;
bmp_data[92].x = 357; bmp_data[92].y = 688;
bmp_data[93].x = 359; bmp_data[93].y = 684;
bmp_data[94].x = 366; bmp_data[94].y = 673;
bmp_data[95].x = 377; bmp_data[95].y = 658;
bmp_data[96].x = 384; bmp_data[96].y = 649;
bmp_data[97].x = 400; bmp_data[97].y = 628;
bmp_data[98].x = 419; bmp_data[98].y = 606;
bmp_data[99].x = 440; bmp_data[99].y = 583;
bmp_data[100].x = 462; bmp_data[100].y = 563;
bmp_data[101].x = 473; bmp_data[101].y = 554;
bmp_data[102].x = 495; bmp_data[102].y = 538;
bmp_data[103].x = 517; bmp_data[103].y = 527;
bmp_data[104].x = 538; bmp_data[104].y = 520;
bmp_data[105].x = 559; bmp_data[105].y = 517;
bmp_data[106].x = 570; bmp_data[106].y = 517;
bmp_data[107].x = 589; bmp_data[107].y = 520;
bmp_data[108].x = 598; bmp_data[108].y = 522;
bmp_data[109].x = 616; bmp_data[109].y = 531;
bmp_data[110].x = 623; bmp_data[110].y = 537;
bmp_data[111].x = 637; bmp_data[111].y = 552;
bmp_data[112].x = 647; bmp_data[112].y = 572;
bmp_data[113].x = 651; bmp_data[113].y = 583;
bmp_data[114].x = 656; bmp_data[114].y = 608;
bmp_data[115].x = 657; bmp_data[115].y = 621;
bmp_data[116].x = 656; bmp_data[116].y = 649;
bmp_data[117].x = 654; bmp_data[117].y = 663;
bmp_data[118].x = 648; bmp_data[118].y = 692;
bmp_data[119].x = 638; bmp_data[119].y = 722;
bmp_data[120].x = 631; bmp_data[120].y = 736;
bmp_data[121].x = 616; bmp_data[121].y = 764;
bmp_data[122].x = 598; bmp_data[122].y = 791;
bmp_data[123].x = 588; bmp_data[123].y = 804;
bmp_data[124].x = 566; bmp_data[124].y = 830;
bmp_data[125].x = 543; bmp_data[125].y = 853;
bmp_data[126].x = 521; bmp_data[126].y = 874;
bmp_data[127].x = 510; bmp_data[127].y = 883;
bmp_data[128].x = 490; bmp_data[128].y = 900;
bmp_data[129].x = 471; bmp_data[129].y = 913;
bmp_data[130].x = 454; bmp_data[130].y = 923;
bmp_data[131].x = 447; bmp_data[131].y = 926;
bmp_data[132].x = 433; bmp_data[132].y = 931;
bmp_data[133].x = 421; bmp_data[133].y = 935;
bmp_data[134].x = 409; bmp_data[134].y = 936;
bmp_data[135].x = 400; bmp_data[135].y = 937;
bmp_data[136].x = 396; bmp_data[136].y = 937;
bmp_data[137].x = 392; bmp_data[137].y = 937;
bmp_data[138].x = 385; bmp_data[138].y = 937;
bmp_data[139].x = 379; bmp_data[139].y = 936;
bmp_data[140].x = 376; bmp_data[140].y = 936;
bmp_data[141].x = 372; bmp_data[141].y = 935;
bmp_data[142].x = 370; bmp_data[142].y = 934;
bmp_data[143].x = 366; bmp_data[143].y = 934;
bmp_data[144].x = 363; bmp_data[144].y = 933;
bmp_data[145].x = 359; bmp_data[145].y = 933;
bmp_data[146].x = 358; bmp_data[146].y = 932;
bmp_data[147].x = 355; bmp_data[147].y = 932;
bmp_data[148].x = 354; bmp_data[148].y = 932;
bmp_data[149].x = 353; bmp_data[149].y = 932;
bmp_data[150].x = 351; bmp_data[150].y = 932;
bmp_data[151].x = 351; bmp_data[151].y = 931;
bmp_data[152].x = 350; bmp_data[152].y = 931;
bmp_data[153].x = 349; bmp_data[153].y = 930;
bmp_data[154].x = 348; bmp_data[154].y = 930;
bmp_data[155].x = 348; bmp_data[155].y = 930;
bmp_data[156].x = 347; bmp_data[156].y = 929;
bmp_data[157].x = 346; bmp_data[157].y = 929;
bmp_data[158].x = 345; bmp_data[158].y = 929;
bmp_data[159].x = 344; bmp_data[159].y = 929;
bmp_data[160].x = 343; bmp_data[160].y = 928;
bmp_data[161].x = 342; bmp_data[161].y = 928;
bmp_data[162].x = 341; bmp_data[162].y = 928;
bmp_data[163].x = 340; bmp_data[163].y = 928;
bmp_data[164].x = 339; bmp_data[164].y = 928;
bmp_data[165].x = 339; bmp_data[165].y = 927;
bmp_data[166].x = 338; bmp_data[166].y = 927;
bmp_data[167].x = 339; bmp_data[167].y = 927;
bmp_data[168].x = 340; bmp_data[168].y = 927;
bmp_data[169].x = 342; bmp_data[169].y = 927;
bmp_data[170].x = 343; bmp_data[170].y = 927;
bmp_data[171].x = 346; bmp_data[171].y = 925;
bmp_data[172].x = -1; bmp_data[172].y = -1;
	/********************************************/
	
	SigCap2BMP(imageBitmap, bmp_data, 173,12);
	
	return 0;
}
 
