# Bitmap Files from Coordinates

A bitmap file or **BMP file format**, sometimes called *bitmap* or **DIB 
file 
format** (_for device-independent bitmap_), is an image file format used 
to 
store bitmap digital images, especially on Microsoft Windows and OS/2 
operating systems.

Many older graphical user interfaces used bitmaps in their built-in 
graphics subsytems; for example, the Microsoft Windows and OS/2 
platforms' GDI subsytem, where the specific format used is the _Windows 
and OS/2 bitmap file format_, usually named with the file extension of 
`.BMP` or `.DIB`.

## File Structure

The bitmap image file consists of fixed-size structures(headers) as well 
as variable-size structures appearing in a predetermined sequence. Many 
different versions of some of these structures can appear in the file, 
due to the long evolution of this file format.

The BMP file is composed of structures in the following order:

|Structure Name|Size|Purpose|Comments|
|--------------|----|-------|--------|
|Bitmap File Header|14 bytes|To store general information about the bitmap image file|Not needed after the file is loaded in memory|
|DIB header|Fixed-size(40bytes)|To store detailed information about the bitmap image and define the pixel format|Inmediately follows the Bitmap file header|
|Color Table|Variable-size|To define colors used by the bitmap image data(Pixel array)|Mandatory for **color depths** <= 8bits|
|Pixel Array|variable-size|Structure alignment|An artifact of the File offset to Pixel array in the Bitmap file header|


## Bitmap file header

This block of bytes is at the start of the file and is used to identify 
the file. A typical application reads this block first to ensure that 
the file is actually a BMP file and that it is not damaged. The first 2 
bytes of the BMP file format are the character "B" then the character 
"M" in ASCII encoding. All of the integer values are stored in 
little-endian format (i.e. least-significant byte first).

|Offset hex|Offset dec|Size|Purpose|
|----------|----------|----|-------|
|00|0|2 bytes|The header field used to identify the BMP and DIB file is 0x42 0x4D in hexadecimal, same as BM in ASCII.| 
|02|2|4 bytes|The size of the BMP file in bytes|
|06|6|2 bytes|Reserved; actual value depends on the application that creates the image|
|08|8|2 bytes|Reserved; actual value depends on the application that creates the image|
|0A|10|4 bytes|	The offset, i.e. starting address, of the byte where the bitmap image data (pixel array) can be found.|

## DIB header (bitmap information header)

This block of bytes tells the application detailed information about the 
image, which will be used to display the image on the screen. The block 
also matches the header used internally by Windows and OS/2 and has 
several different variants. All of them contain a dword (32-bit) field, 
specifying their size, so that an application can easily determine which 
header is used in the image. The reason that there are different headers 
is that Microsoft extended the DIB format several times. The new 
extended headers can be used with some GDI functions instead of the 
older ones, providing more functionality. Since the GDI supports a 
function for loading bitmap files, typical Windows applications use that 
functionality. One consequence of this is that for such applications, 
the BMP formats that they support match the formats supported by the 
Windows version being run. See the table below for more information.

|Offset(hex)|Offset(dec)|Size(bytes)|Purpose|
|-----------|-----------|-----------|-------|
|0E|14|4|The size of this header (40bytes)|
|12|18|4|the bitmap width in pixels (signed integer)|
|16|22|4|the bitmap height in pixels (signed integer)|
|1A|26|2|the number of color planes (must be 1)|
|1C|28|2|the number of bits per pixel, which is the color depth of the image. Typical values are 1, 4, 8, 16, 24 and 32|
|1E|30|4|the compression method being used. See the next table for a list of possible values|
|22|34|4|the image size. This is the size of the raw bitmap data; a dummy 0 can be given for BI_RGB bitmaps|
|26|38|4|the horizontal resolution of the image. (pixel per metre, signed integer)|
|2A|42|4|the vertical resolution of the image. (pixel per metre, signed integer)|
|2E|46|4|the number of colors in the color palette, or 0 to default to 2|
|32|50|4|the number of important colors used, or 0 when every color is important; generally ignored|


## Pixel Storage

The bits representing the bitmap pixels are packed in rows. The size of 
each row is rounded up to a multiple of 4 bytes (a 32-bit DWORD) by 
padding.
For images with height > 1, multiple padded rows are stored 
consecutively, forming a Pixel Array.

The total number of bytes necessary to store one row of pixels can be 
calculated as:

```math
RowSize = [BitsPerPixel.ImageWidth + 31 / 32].4, 
```
_ImageWidth_ is expressed in pixels, note the special parentheses.

The total amount of bytes necessary to store an array of pixels in an n 
bits per pixel (bpp) image, with 2n colors, can be calculated by 
accounting for the effect of rounding up the size of each row to a 
multiple of 4 bytes, as follows:

```math
PixelArraySize = RowSize.|ImageWidth|
```
_ImageHeight_ is expressed in pixels. The absolute value is necessary 
because ImageHeight can be negative.

## Pixel array (bitmap data)

The pixel array is a block of 32-bit DWORDs, that describes the image  
pixel by pixel. Usually pixels are stored "upside-down" with respect to 
normal image raster scan order, starting in the lower left corner, going 
from left to right, and then row by row from the bottom to the top of 
the image.

In the original OS/2 DIB, the only four legal values of color depth were 
1, 4, 8, and 24 bits per pixel (bpp). Contemporary DIB Headers allow 
pixel formats with 1, 2, 4, 8, 16, 24 and 32 bits per pixel (bpp).

Padding bytes (not necessarily 0) must be appended to the end of the 
rows in order to bring up the length of the rows to a multiple of four 
bytes. When the pixel array is loaded into memory, each row must begin 
at a memory address that is a multiple of 4. This address/offset 
restriction is mandatory only for Pixel Arrays loaded in memory. For 
file storage purposes, only the size of each row must be a multiple of 4 
bytes while the file offset can be arbitrary. A 24-bit bitmap with 
Width=1, would have 3 bytes of data per row (blue, green, red) and 1 
byte of padding, while Width=2 would have 2 bytes of padding, Width=3 
would have 3 bytes of padding, and Width=4 would not have any padding at 
all.

## Pixel format

* The 1-bit per pixel (1bpp) format supports 2 distinct colors, (for 
example: black and white). The pixel values are stored in each bit, with 
the first (left-most) pixel in the most-significant bit of the first 
byte.Each bit is an index into a table of 2 colors. An unset bit 
will refer to the first color table entry, and a set bit will refer to 
the last (second) color table entry.
* The 2-bit per pixel (2bpp) format supports 4 distinct colors and 
stores 4 pixels per 1 byte, the left-most pixel being in the two most 
significant bits (Windows CE only:). Each pixel value is a 2-bit 
index into a table of up to 4 colors.
*The 4-bit per pixel (4bpp) format supports 16 distinct colors and 
stores 2 pixels per 1 byte, the left-most pixel being in the more 
significant nibble. Each pixel value is a 4-bit index into a table of 
up to 16 colors.
*The 8-bit per pixel (8bpp) format supports 256 distinct colors and 
stores 1 pixel per 1 byte. Each byte is an index into a table of up to 
256 colors.
* The 16-bit per pixel (16bpp) format supports 65536 distinct colors and 
stores 1 pixel per 2-byte WORD. Each WORD can define the alpha, red, 
green and blue samples of the pixel.
* The 24-bit pixel (24bpp) format supports 16,777,216 distinct colors 
and stores 1 pixel value per 3 bytes. Each pixel value defines the red, 
green and blue samples of the pixel (8.8.8.0.0 in RGBAX notation). 
Specifically, in the order: blue, green and red (8 bits per each 
sample).
* The 32-bit per pixel (32bpp) format supports 4,294,967,296 distinct 
colors and stores 1 pixel per 4-byte DWORD. Each DWORD can define the 
alpha, red, green and blue samples of the pixel.

## Application: Generate Bitmap files of 24 bpp and 1bpp Depth from Coordinates

The present work show how to generate bitmap files of 24bpp and 1bpp 
depth from cartesian coordinates. As we know, the pixel array data is a 
matrix where will allocate the points that will print in the image.
Now, how to get this points or data that will allocate in my image? Well, the 
application for this work is get these points from a touch screen device  
where we will draw a signature. The screen capture these points and 
allocate these in a structure defined with two fields: first, the 
component of horizontal axis and the other, the component of vertical 
axis.

```c

struct bmpdata{
	int x; //horizontal axis's component
	int y; //vertical axis's component
};

```
Then, if we know the number of points or coordinates, calculate the 
maximum and minimum points to allignment the image. Marking these points 
with dummy variable(or color data), and store their in a matrix data. 
But, these are cartesian coordinates, then if we put them in a matrix 
and write in the image file, then we will reproduce the image, it will 
flipped. Why?, because the indexes of matrix not matching with the 
cartesian coordinates, but only we will change and put "upside-down" the 
indexes in matrix. 

Now, we describe both cases for 1bpp and 24bpp depth. 

## Bitmap Files 1bpp

When we operate with bitmap files of 1bpp depth, we have a single 
problem. What is the pattern to print the bits per pixel in the image?, 
well, in bit level we have a typical pattern. We know that a byte store 
eightt bits, but also know that we have a 256 combinations if store it 
in a vector of size 8. In the bitmap file is very important the 
dimensions of image, because the width will define the number of bytes 
per row. Also, we must to know that a bitmap file be write from left to 
right. 

## Bitmap Files 24bpp
 
 


